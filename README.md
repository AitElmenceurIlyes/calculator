# Calculator

This Maven project is a Swing calculator
It use different **design patern** and a **MVC architecture**
**Abstraction** was used in order to plan futher devellopment for this app such as a scientific mode *(not implemented)*

## Methods used

    - MVC
    - Singleton
    - Observer
    - Interfaces
    - SOLID
    - Testing

## Contributor

* Ait Elmenceur Ilyès
