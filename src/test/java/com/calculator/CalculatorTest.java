package com.calculator;



import com.calculator.controler.Calculator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class CalculatorTest {
    private Calculator calculator;
    @Before public void initialize() {
       calculator = new Calculator(Double.parseDouble("3"),
       Double.parseDouble("6"), null);
    }
    /** **/
    @Test
    public void sumTest() {
        calculator.setOperator("+");
        Double expDouble = Double.valueOf(9);
        calculator.Compute();
        Assert.assertEquals(expDouble.doubleValue(), calculator.getResult(), 0);
    }
    /** **/
    @Test
    public void subTest() {
        calculator.setOperator("-");
        Double expDouble = Double.valueOf(-3);
        calculator.Compute();
        Assert.assertEquals(expDouble.doubleValue(), calculator.getResult(), 0);
    }
    /** **/
    @Test
    public void multiplyTest() {
        calculator.setOperator("*");
        Double expDouble = Double.valueOf(3 * 6);
        calculator.Compute();
        Assert.assertEquals(expDouble.doubleValue(), calculator.getResult(), 0);
    }
    /** **/
    @Test
    public void divideTest() {
        calculator.setOperator("/");
        Double expDouble = Double.valueOf(3 / 6);
        calculator.Compute();
        Assert.assertEquals(expDouble.doubleValue(), calculator.getResult(), 1);
    }
    /** **/
    @Test
    public void modTest() {
        calculator.setOperator("mod");
        Double expDouble = Double.valueOf(3 % 6);
        calculator.Compute();
        Assert.assertEquals(expDouble.doubleValue(), calculator.getResult(), 0);
    }

}
