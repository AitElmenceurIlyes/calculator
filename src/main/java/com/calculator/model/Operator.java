package com.calculator.model;

public enum Operator {
    SUM("+"),
    SUBSTRACT("-"),
    MULTIPLY("*"),
    DIVIDE("/"),
    MOD("mod");
    private String operator;

    private Operator(final String op) {
        this.operator = op;
    }

    public static Operator GetOperator(final String oString) {
        for (Operator c : values()) {
            if (c.operator == oString) {
                return c;
            }
        }
        return null;
    }
}
