package com.calculator.controler.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

import com.calculator.controler.Calculator;

public class EnterListener implements ActionListener {
    private static JLabel jOperand1;
    private static JLabel jOperand2;
    private static JLabel jOperator;
    private static JLabel jResult;
    private static Calculator calculator = new Calculator(null,null,null);
/**
 * Listener for the button that validate the computation
 * @param jOperand1JLabel
 * @param jOperand2JLabel
 * @param jOperatorJLabel
 * @param jResultJLabel
 * @param c
 */
    public EnterListener(final JLabel jOperand1JLabel,
    final JLabel jOperand2JLabel, final JLabel jOperatorJLabel,
    final JLabel jResultJLabel) {
        jOperand1 = jOperand1JLabel;
        jOperand2 = jOperand2JLabel;
        jOperator = jOperatorJLabel;
        jResult = jResultJLabel;
    }

    
    /** 
     * @param e
     */
    @Override
    public final void actionPerformed(final ActionEvent e) {
        if (!jOperand1.getText().equals("")
                && !jOperand2.getText().equals("")
                && !jOperator.getText().equals("")) {
            calculator.setOperand1Double(Double
                    .parseDouble(jOperand1.getText()));
            calculator.setOperand2Double(
                    Double.parseDouble(jOperand2.getText()));
            calculator.setOperator(jOperator.getText());
            try {
                calculator.Compute();
                jResult.setText("" + (calculator.getResult()));
            }
            catch (Exception exception) {
                jResult.setText("" + exception.toString());

            }
            finally {
                jOperand1.setText("");
                jOperand2.setText("");
               jOperator.setText("");
            }
        }
    }
}
