package com.calculator.controler.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;

public class EraseListener implements ActionListener {
    private static JLabel jOperand1;
    private static JLabel jOperand2;
    private static JLabel jOperator;
    private static JLabel jResult;
/**
 * Listener for the button which erase every entries
 * @param jOperand1JLabel
 * @param jOperand2JLabel
 * @param jOperatorJLabel
 * @param jResultJLabel
 */
    public EraseListener(final JLabel jOperand1JLabel,
    final JLabel jOperand2JLabel, final JLabel jOperatorJLabel,
    final JLabel jResultJLabel) {
        jOperand1 = jOperand1JLabel;
        jOperand2 = jOperand2JLabel;
        jOperator = jOperatorJLabel;
        jResult = jResultJLabel;
    }

    
    /** 
     * @param e
     */
    @Override
    public void actionPerformed(final ActionEvent e) {
        jOperand1.setText("");
        jOperator.setText("");
        jOperand2.setText("");
        jResult.setText("");
    }
}
