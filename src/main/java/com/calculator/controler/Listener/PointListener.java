package com.calculator.controler.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

public class PointListener implements ActionListener {
    private static JLabel jOperand1;
    private static JLabel jOperand2;
    private static JLabel jOperator;
/**
 * Listener for button which set the point of a decimal
 * @param jOperand1JLabel
 * @param jOperand2JLabel
 * @param jOperatorJLabel
 */
    public PointListener(final JLabel jOperand1JLabel,
     final JLabel jOperand2JLabel, final JLabel jOperatorJLabel) {
        jOperand1 = jOperand1JLabel;
        jOperand2 = jOperand2JLabel;
        jOperator = jOperatorJLabel;
    }

    
    /** 
     * @param e
     */
    @Override
    public final void actionPerformed(final ActionEvent e) {
        JButton b = (JButton) e.getSource();
        System.out.println(b.getText());
        if (jOperator.getText().equals("")) {
            if (jOperand1.getText().equals("")) {
                jOperand1.setText(b.getText());
            } else if (!jOperand1.getText().contains(".")) {
                jOperand1.setText(jOperand1.getText() + b.getText());
            }
        } else {
            if (jOperand2.getText().equals("")) {
                jOperand2.setText(b.getText());
            } else if (!jOperand2.getText().contains(".")) {
                jOperand2.setText(jOperand2.getText() + b.getText());
            }

        }
    }
}
