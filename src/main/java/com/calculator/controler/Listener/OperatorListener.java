package com.calculator.controler.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

public class OperatorListener implements ActionListener {
    private static JLabel jOperand1;
    private static JLabel jOperator;
    private static JLabel jResult;
/**
 * Listener for operator buttons
 * @param jOperand1lJLabel
 * @param jOperatorJLabel
 * @param jResultJLabel
 */
    public OperatorListener(final JLabel jOperand1lJLabel,
    final JLabel jOperatorJLabel, final JLabel jResultJLabel) {
        jOperand1 = jOperand1lJLabel;
        jOperator =  jOperatorJLabel;
        jResult = jResultJLabel;
    }

    
    /** 
     * @param e
     */
    @Override
    public final void actionPerformed(final ActionEvent e) {
        if (!jResult.getText().equals("")
                &&
                jOperand1.getText().equals("")) {
            jOperand1.setText(jResult.getText());
            jResult.setText("");
        }
        if (!jOperand1.getText().equals("")) {
            JButton b = (JButton) e.getSource();
            System.out.println(b.getText());
           jOperator.setText(b.getText());
        }
    }

}
