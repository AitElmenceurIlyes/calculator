package com.calculator.controler;

import com.calculator.model.Operator;

public class Calculator implements ICompute{
    private Double operand2Double;
    private String operator;
    private Double result;
    private Double operand1Double;
    /**
     * Constructor
     * @param operand1Double
     * @param operand2Double
     * @param operator
     */
    public Calculator(Double operand1Double, Double operand2Double,
            String operator) {
        this.operand1Double = operand1Double;
        this.operand2Double = operand2Double;
        this.operator = operator;
    }

    /**
     * @return double
     */
    public double getResult() {
        return result;
    }

    /**
     * @return Double
     */
    public Double getOperand1Double() {
        return operand1Double;
    }

    /**
     * @param operand1Double
     */
    public void setOperand1Double(Double operand1Double) {
        this.operand1Double = operand1Double;
    }

    /**
     * @return Double
     */
    public Double getOperand2Double() {
        return operand2Double;
    }

    /**
     * @param operand2Double
     */
    public void setOperand2Double(Double operand2Double) {
        this.operand2Double = operand2Double;
    }

    /**
     * @return String
     */
    public String getOperator() {
        return operator;
    }

    /**
     * @param operator
     */
    public void setOperator(String operator) {
        this.operator = operator;
    }
/**
 * Compute the result
 */
    public void Compute() throws ArithmeticException {
        if (operand1Double == null) {
            operand1Double = result;
        }

        switch (Operator.GetOperator(operator)) {
            case SUM:
                result = operand1Double + operand2Double;
                break;
            case SUBSTRACT:
                result = operand1Double - operand2Double;
                break;
            case DIVIDE:
                try {
                    result = operand1Double / operand2Double;
                } catch (ArithmeticException e) {
                    e.printStackTrace();
                    throw  e;
                }
                break;
            case MULTIPLY:
                try {
                    result = operand1Double * operand2Double;
                } catch (ArithmeticException e) {
                    e.printStackTrace();
                    throw  e;
                }
                break;
            case MOD:
                result = operand1Double % operand2Double;
                break;
            default:
                break;
        }
        operand1Double = null;
        operand2Double = null;
    }

}
