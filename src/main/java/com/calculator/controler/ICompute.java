package com.calculator.controler;

public interface ICompute {
    /**
     * Define general method for a calculator,
     * useful for futher developement (for exemple, scientific calculator).
     */
    void Compute();
}
