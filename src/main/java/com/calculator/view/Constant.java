package com.calculator.view;

import java.awt.Color;


public class Constant {
    public static final Color PANELRESULT_BACKGROUND = new Color(25, 41, 43);
    public static final Color PANELRESULT_FOREGROUND = Color.WHITE;
    public static final Color ESSENSIALPANEL_BACKGROUND = new Color(74, 74, 74);
    public static final Color ESSENSIALPANEL_FOREGROUND = Color.WHITE;
    public static final Color ENTER_BUTTON_BACKGROUND = new Color(186, 91, 39);
    public static final Color ENTER_BUTTON_FOREGROUND = Color.WHITE;
    public static final int HEIGHTGAP = 1;
    public static final int WIDTHGAP = 1;
}
