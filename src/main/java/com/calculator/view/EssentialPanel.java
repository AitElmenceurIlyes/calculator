package com.calculator.view;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.calculator.controler.Listener.EnterListener;
import com.calculator.controler.Listener.EraseListener;
import com.calculator.controler.Listener.MinusListener;
import com.calculator.controler.Listener.NumberListener;
import com.calculator.controler.Listener.PointListener;

import java.util.ArrayList;
import java.awt.GridLayout;
import java.awt.ComponentOrientation;
import java.awt.Color;
public final class EssentialPanel {
    /** utilitary class default constructor. **/
    private EssentialPanel() {
    }

    /**
     * Panel which contain numbers
     * @param jOperand1
     * @param jOperand2
     * @param jOperator
     * @param jResult
     * @return JPanel
     */
    public static JPanel buildNumberPanel(final JLabel jOperand1,
            final JLabel jOperand2,
            final JLabel jOperator, final JLabel jResult) {
        JPanel numberPanel = new JPanel();
        numberPanel.setLayout(new GridLayout(3,3,
        Constant.HEIGHTGAP,
        Constant.WIDTHGAP));
        ArrayList<JButton> numberlist = new ArrayList<JButton>();
        numberlist.add(new JButton("9"));
        numberlist.add(new JButton("8"));
        numberlist.add(new JButton("7"));
        numberlist.add(new JButton("6"));
        numberlist.add(new JButton("5"));
        numberlist.add(new JButton("4"));
        numberlist.add(new JButton("3"));
        numberlist.add(new JButton("2"));
        numberlist.add(new JButton("1"));
        numberPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        for (JButton j : numberlist) {
            numberPanel.add(j);
            j.setBackground(Constant.ESSENSIALPANEL_BACKGROUND);
            j.setForeground(Constant.ESSENSIALPANEL_FOREGROUND);
            j.addActionListener(new NumberListener(jOperand1,
            jOperand2, jOperator));
        }
        return numberPanel;
    }

    /**
     * @param jOperand1
     * @param jOperand2
     * @param jOperator
     * @param jResult
     * @return JPanel
     */
    public static JPanel buildzeroPanel(
            final JLabel jOperand1, final JLabel jOperand2,
            final JLabel jOperator, final JLabel jResult) {
        JPanel zeroPanel = new JPanel();
        zeroPanel.setLayout(new GridLayout(1, 3,
        Constant.HEIGHTGAP,
        Constant.WIDTHGAP));
        // zeroPanel.setPreferredSize(new Dimension(20,20));
        JButton button0 = new JButton("0");
        button0.addActionListener(new NumberListener(jOperand1,
        jOperand2, jOperator));
        button0.setBackground(Constant.ESSENSIALPANEL_BACKGROUND);
        button0.setForeground(Constant.ESSENSIALPANEL_FOREGROUND);
        zeroPanel.add(button0);
        JButton buttonPoint = new JButton(".");
        buttonPoint.addActionListener(new PointListener(jOperand1,
        jOperand2, jOperator));
        buttonPoint.setBackground(Constant.ESSENSIALPANEL_BACKGROUND);
        buttonPoint.setForeground(Constant.ESSENSIALPANEL_FOREGROUND);
        zeroPanel.add(buttonPoint);
        JButton buttonMinus = new JButton("(-)");
        buttonMinus.addActionListener(new MinusListener(jOperand1,
        jOperand2, jOperator));
        buttonMinus.setBackground(Constant.ESSENSIALPANEL_BACKGROUND);
        buttonMinus.setForeground(Constant.ESSENSIALPANEL_FOREGROUND);
        zeroPanel.add(buttonMinus);
        JButton buttonErase = new JButton("E");
        buttonErase.addActionListener(new EraseListener(jOperand1, jOperand2,
        jOperator, jResult));
        buttonErase.setBackground(Constant.ESSENSIALPANEL_BACKGROUND);
        buttonErase.setForeground(Constant.ESSENSIALPANEL_FOREGROUND);
        zeroPanel.add(buttonErase);
        JButton benter = new JButton("enter");
        benter.addActionListener(new EnterListener(jOperand1, jOperand2,
        jOperator, jResult));
        benter.setBackground(Constant.ENTER_BUTTON_BACKGROUND);
        benter.setForeground(Constant.ENTER_BUTTON_FOREGROUND);
        zeroPanel.add(benter);
        zeroPanel.setBackground(Color.WHITE);
        return zeroPanel;
    }

}
