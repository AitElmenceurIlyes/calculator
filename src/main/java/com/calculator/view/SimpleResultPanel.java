package com.calculator.view;

import javax.swing.JLabel;
import javax.swing.JPanel;

public final class SimpleResultPanel implements IResultPanel {
    /** Singleton.**/
    private static SimpleResultPanel instance = null;
    private SimpleResultPanel() {
    }
    /**
     *
     * @return instance
     */
    public static SimpleResultPanel getInstance() {
       if (instance == null) {
          instance = new SimpleResultPanel();
       }
       return instance;
    }


    /**
     * Build the resut panel
     * @param jOperand1
     * @param jOperand2
     * @param jOperator
     * @param jResult
     * @return JPanel
     */
    public JPanel buildResultPanel(final JLabel jOperand1,
    final JLabel jOperand2, final JLabel jOperator, final JLabel  jResult) {
        JPanel resultPanel = new JPanel();
        resultPanel.add(jOperand1);
        resultPanel.add(jOperator);
        resultPanel.add(jOperand2);
        JLabel equLabel = new JLabel("=");
        equLabel.setForeground(Constant.PANELRESULT_FOREGROUND);
        resultPanel.add(equLabel);
        resultPanel.add(jResult);
        resultPanel.setBackground(Constant.PANELRESULT_BACKGROUND);
        jOperand1.setForeground(Constant.PANELRESULT_FOREGROUND);
        jOperand2.setForeground(Constant.PANELRESULT_FOREGROUND);
        jOperator.setForeground(Constant.PANELRESULT_FOREGROUND);
        jResult.setForeground(Constant.PANELRESULT_FOREGROUND);

        return resultPanel;
    }
   }
