package com.calculator.view;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.calculator.controler.Listener.OperatorListener;

import java.awt.GridLayout;

import java.util.ArrayList;

public final class SimplePanelOperator implements IOperatorPanel {
    /** Singleton.**/
    private static SimplePanelOperator instance = null;
    private SimplePanelOperator() {
    }
    /**
     *
     * @return instance
     */
    public static SimplePanelOperator getInstance() {
       if (instance == null) {
          instance = new SimplePanelOperator();
       }
       return instance;
    }

    /**
     * Build the Operator panel.
     * @param jOperand1
     * First Operand.
     * @param jOperand2
     * Second Operand.
     * @param jOperator
     * Operator.
     * @param jResult
     * Display the result.
     * @return JPanel
     * return the constructed panel.
     */
    public JPanel buildOperatorPanel(final JLabel jOperand1,
            final JLabel jOperand2, final JLabel jOperator,
            final JLabel jResult) {
        JPanel operatorPanel = new JPanel();
        operatorPanel.setLayout(new GridLayout(5, 1,
        Constant.HEIGHTGAP,
        Constant.WIDTHGAP));
        ArrayList<JButton> opeArrayList = new ArrayList<JButton>();
        opeArrayList.add(new JButton("+"));
        opeArrayList.add(new JButton("-"));
        opeArrayList.add(new JButton("*"));
        opeArrayList.add(new JButton("/"));
        opeArrayList.add(new JButton("mod"));
        for (JButton j : opeArrayList) {
            j.setBackground(Constant.ESSENSIALPANEL_BACKGROUND);
            j.setForeground(Constant.ESSENSIALPANEL_FOREGROUND);
            operatorPanel.add(j);

            j.addActionListener(new OperatorListener(jOperand1,
            jOperator, jResult));
        }
        return operatorPanel;
    }

}
