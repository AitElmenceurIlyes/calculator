package com.calculator.view;

import javax.swing.JLabel;
import javax.swing.JPanel;

public interface IOperatorPanel {
    /**
     * This interface define the method
     * that each operator panel(ex: scientific or simple)
     * must implement, usefull for futher improvement.
     * @param jOperand1
     * @param jOperand2
     * @param jOperator
     * @param jResult
     * @return JPanel
     */
    JPanel buildOperatorPanel(JLabel jOperand1, JLabel jOperand2,
    JLabel jOperator, JLabel jResult);
}
