package com.calculator.view;


import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GUI {
    /** main frame. **/
    private JFrame frame;
    /** JAvaswing object representing the first operand. **/
    private static JLabel jOperand1 = new JLabel("");
    /** JAvaswing object representing the second operand. **/
    private static JLabel jOperand2 = new JLabel("");
    /** JAvaswing object representing the operator. **/
    private static JLabel jOperator = new JLabel("");
    /** JAvaswing object representing the first operand. **/
    private static JLabel jResult = new JLabel("");

    /** Construct the GUI. **/
    public GUI() {
        buildFrame();
    }

    /**
     * @param args
     */
    public static void main(final String[] args) {
        new GUI();
    }

    /** Build the main frame. **/
    public final void buildFrame() {
        frame = new JFrame("Calculator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(new Dimension(200, 200));
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        frame.add(mainPanel);
        mainPanel.add(EssentialPanel
                .buildNumberPanel(jOperand1, jOperand2, jOperator, jResult),
                BorderLayout.CENTER);
        mainPanel.add(SimplePanelOperator.getInstance()
        .buildOperatorPanel(jOperand1, jOperand2, jOperator, jResult),
        BorderLayout.EAST);
        mainPanel.add(EssentialPanel.buildzeroPanel(
                jOperand1, jOperand2, jOperator, jResult),
                BorderLayout.SOUTH);
        mainPanel.add(SimpleResultPanel.getInstance()
        .buildResultPanel(jOperand1, jOperand2, jOperator, jResult),
        BorderLayout.NORTH);
        frame.pack();
        frame.setVisible(true);
    }
}
